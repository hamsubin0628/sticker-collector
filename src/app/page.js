'use client'

import { useDispatch, useSelector } from "react-redux";
import { pickSticker } from "@/lib/features/sticker/stickerSlice";

export default function Home() {
    const { stickerList } = useSelector(state => state.sticker);
    const dispatch = useDispatch();

    const handleOpenSticker = () => {
        dispatch(pickSticker());
    }

    return (
        <main className="fn-app">
            <div className="fn-title-section">
                <p className="fn-sub-title">스티커 랜덤 뽑기</p>
                <h1 className="fn-title">🎈 마이포카쮸 <p className="fn-title2">짱구 스티커 🎈</p></h1>
                <button className="fn-btn" onClick={handleOpenSticker}>마이포카쮸 먹기</button>
            </div>
            <p className="fn-what">뭐가 나올까?</p>
            <ul className="fn-random-section">
                {stickerList.map((sticker, index) => (
                    <div key={index} className="fn-random-card">{sticker.isPick ?
                        <div className="fn-card-section">
                            <img src={sticker.imgUrl} className="fn-random-image" />
                            <p className="fn-character-name">{sticker.name}</p>
                        </div> : '?'}</div>
                ))}
            </ul>
        </main>
    );
}
