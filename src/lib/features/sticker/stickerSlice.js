import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    stickerList: [
        { name: '짱구', imgUrl: './assets/images/sinjjang.png', isPick: false },
        { name: '수지', imgUrl: './assets/images/sooji.png', isPick: false },
        { name: '흰둥이', imgUrl: './assets/images/white.png', isPick: false },
        { name: '오수', imgUrl: './assets/images/ohsoo.png', isPick: false },
        { name: '맹구', imgUrl: './assets/images/maenggu.png', isPick: false },
        { name: '훈발롬', imgUrl: './assets/images/hoonbal.png', isPick: false },
        { name: '건담로봇', imgUrl: './assets/images/gundam.png', isPick: false },
        { name: '공룡인형', imgUrl: './assets/images/dino.png', isPick: false },
        { name: '철수', imgUrl: './assets/images/cheolsoo.png', isPick: false },
        { name: '액션가면', imgUrl: './assets/images/action_mask.png', isPick: false },
    ]
}

const stickerSlice = createSlice({
    name: 'sticker',
    initialState,
    reducers: {
        pickSticker: (state) => {
            // 0부터 stickerList.length - 1 => outBoundRange 에러
            const randomIndex = Math.floor(Math.random() * state.stickerList.length)
            state.stickerList[randomIndex].isPick = true
        }
    }
})

export const { pickSticker } = stickerSlice.actions
export default stickerSlice.reducer