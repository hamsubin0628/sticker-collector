import { configureStore } from '@reduxjs/toolkit'
import StickerReducer from "@/lib/features/sticker/stickerSlice";

export default configureStore({
    reducer: {
        sticker: StickerReducer,
    },
})